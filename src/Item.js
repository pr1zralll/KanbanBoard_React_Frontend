import React, { Component } from 'react';

class Item extends Component {
  constructor (props) {
    super(props);
    this.state = {typing:false};
    this.onButton = this.onButton.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.renderOk = this.renderOk.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onChange2 = this.onChange2.bind(this);
    this.item = Object.assign({}, this.props.item);
  }
  onButton(){
    this.props.removeItem(this.props.item.id);
  }
  onEdit(){
    if(this.state.typing)
            this.onSubmit();
    this.setState({typing: !this.state.typing});
  }
  onSubmit(){
    var item = this.item;
    this.props.onEdit(item);
  }

  renderOk() {
    this.item = Object.assign({}, this.props.item);
    return (
      <div className='Item' draggable='true' onDragStart={this.onDragStart} id={this.props.item.id}>
        <h4>{"Task #"+this.props.item.id}</h4>
        <h2>{this.props.item.title}</h2>
        <h3>{this.props.item.description}</h3>
        <button className='TaskCloseButton' onClick={this.onButton}>remove</button>
        <button className='TaskEditButton' onClick={this.onEdit}>edit</button>
      </div>
      );
  }
  onChange(event){
    this.item.title = event.target.value;
    this.setState({typing:true});
  }
  onChange2(event){
    this.item.description = event.target.value;
    this.setState({typing:true});
  }
  renderInput() {
    return (
      <div className='Item'>
        <h4>{"Task #"+this.props.item.id}</h4>
        <input type='text' onChange={this.onChange} value={this.item.title} />
        <input type='text' onChange={this.onChange2} value={this.item.description} />
        <button className='TaskCloseButton' onClick={this.onEdit}>ok</button>
      </div>
      );
  }
  render() {
    if(this.state.typing)
      return this.renderInput();
    else
      return this.renderOk();
  }
  onDragStart(event){
    event.dataTransfer.setData("text", event.target.id);
  }
}

export default Item;

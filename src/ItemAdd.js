import React, { Component } from 'react';

class ItemAdd extends Component {
  constructor (props) {
    super(props);
    this.state = {typing:false,input1:'',input2:''};
    this.onButton = this.onButton.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(){
    var item = {title:'',description:'',status:0};
    item.title = this.state.input1.value;
    item.description = this.state.input2.value;
    this.props.itemAdd(item);
  }
  onButton(){
    if(this.state.typing)
            this.onSubmit();
    this.setState({typing: !this.state.typing});
  }
  renderOk() {
    return (
      <div >
        <button className="TaskAddButton" onClick={this.onButton} >Add Task</button>
      </div>
    );
  }
  renderInput() {
    return (
      <div className="TaskAddButton">
        <input type="text" ref={(input1) => {this.state.input1=input1;}}  placeholder="add a new task..."/>
        <input type="text" ref={(input2) => {this.state.input2=input2;}}  placeholder="decription..."/>
        <button onClick={this.onButton} >ok</button>
      </div>
    );
  }
  render() {
    if(this.state.typing)
      return this.renderInput();
    else
      return this.renderOk();
  }
}

export default ItemAdd;

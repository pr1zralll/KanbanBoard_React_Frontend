import React, { Component } from 'react';
import Item from './Item';
class ListItems extends Component {
  constructor (props) {
    super(props);
    this.getItems = this.getItems.bind(this);
    this.drop = this.drop.bind(this);
  }
  getItems(items){
    return items.map((item,index)=><Item key={index} item={item} removeItem={this.props.removeItem} onEdit={this.props.onEdit} />);
  }
  render() {
    return <div onDrop={this.drop} onDragOver={this.allowDrop} id={this.props.name.length} className={'ListItem '+this.props.name.split(' ').join('')}><h1>{this.props.name}</h1>{this.getItems(this.props.items)}</div>;
  }
  allowDrop(ev) {
    ev.preventDefault();
  }
  drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    //ev.target.appendChild(document.getElementById(data));
    let status = 1;
    if(this.props.name=="Active")
      status = 0;
    if(this.props.name=="Complited")
      status = 2;
      //else 1 in progress
    this.props.onDnD(data,status);
  }
}

export default ListItems;

import React, { Component } from 'react';
import './App.css';
import ItemAdd from './ItemAdd';
import ListItems from './ListItems';
import axios from 'axios';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {items:[]};
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.dndItem = this.dndItem.bind(this);
    this.url = 'http://localhost:57637/api/tasks';

    var self = this;
    axios.get(self.url)
      .then(function (response) {
        self.setState({items: response.data});
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  addItem(item){
    var self = this;
    axios.post(self.url,item)
      .then(function (response) {
        self.state.items.push(response.data);
        self.setState({items: self.state.items});
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  dndItem(id,status){

    var self = this;
    let item;
    self.state.items.forEach(function(element){
      if(id==element.id){
        element.status = status;
        item=element;
      }
    });

    axios.put(self.url+'/'+item.id,item)
      .then(function (response2) {
          self.setState({items: self.state.items});
      })
      .catch(function (error2) {
        console.log(error2);
      });
  }
  editItem(item){
    var self = this;
    axios.put(self.url+'/'+item.id,item)
      .then(function (response) {
        self.state.items.forEach(function(element,i,arr){if(item.id==element.id)arr[i]=item});
        self.setState({items: self.state.items});
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  removeItem(id){
    var self = this;
    axios.delete(self.url+'/'+id)
      .then(function (response) {
        let index;
        self.state.items.forEach(function(item,i){if(item.id==id)index=i});
        self.state.items.splice(index,1);
        self.setState({items: self.state.items});
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  render() {
    return (
      <div id="main" className='App'>
          <ListItems name='Active' items={this.state.items.filter((item)=>item.status==0)} removeItem={this.removeItem} onEdit={this.editItem} onDnD={this.dndItem} />
          <ListItems name='In progress' items={this.state.items.filter((item)=>item.status==1)} removeItem={this.removeItem} onEdit={this.editItem} onDnD={this.dndItem} />
          <ListItems name='Complited' items={this.state.items.filter((item)=>item.status==2)} removeItem={this.removeItem} onEdit={this.editItem} onDnD={this.dndItem} />
          <ItemAdd itemAdd={this.addItem} />
      </div>
    );
  }
}

export default App;
